import React from "react";
// import HelloWorld1 from "./screens/1st-helloWorld/1st-HelloWorldScreen.js";
// import HelloWorld2 from "./screens/2nd-helloWorld/2nd-HelloWorldScreen.js";
// import MomoLogin from "./screens/3rd-momoLoginScreen/3rd-momoLoginScreen.js";
import FacebookLogin from "./screens/4th-FacebookLoginScreen/4th-FacebookLoginScreen.jsx";

export default function App() {
  // return <HelloWorld1 />;
  // return <HelloWorld2 />;
  // return <MomoLogin />;
  return <FacebookLogin />;
}
