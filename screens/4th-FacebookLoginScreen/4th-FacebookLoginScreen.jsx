import React from 'react';
import Constants from "expo-constants";
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, TextInput, Image } from 'react-native';
import { StatusBar } from 'expo-status-bar';
import BannerImage from '../../assets/facebook-banner.jpg';

export default function FacebookLogin() {
  return (
    <>
      <StatusBar style="light" />
      <Image
        source= {BannerImage}
        style={styles.image}
      />
    <SafeAreaView style={styles.safeArea}>
        <View style={styles.container}>
          <View>
            <TextInput
              style={[styles.textInput, styles.phoneInput,]}
              placeholder="Số điện thoại hoặc email"
              placeholderTextColor="#cdcdcf"
              keyboardType="numeric"
              autoFocus={false}
            />
            <TextInput
              style={[styles.textInput, styles.passwordInput,]}
              placeholder="Mật khẩu"
              placeholderTextColor="#cdcdcf"
              keyboardType="numeric"
              autoFocus={false}
              secureTextEntry={true}
            />
            <TouchableOpacity style={styles.loginButton}>
              <Text style={styles.loginText}>Đăng nhập</Text>
            </TouchableOpacity>

            <View style={{ alignItems: 'center'}}>
              <TouchableOpacity>
                <Text style={styles.optionRowText}>Quên mật khẩu ?</Text>
              </TouchableOpacity>

              <TouchableOpacity style={{paddingVertical: 8}}>
                <Text style={styles.optionRowText}>Quay lại</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{alignItems: 'center'}}>
            <View style={styles.footer}>
              <View style={styles.divider}></View>
              <Text style={styles.dividerText}>HOẶC</Text>
              <View style={styles.divider}></View>
            </View>
            <TouchableOpacity style={styles.newAccount}>
              <Text style={{color: "#1077f7"}}>Tạo tài khoản mới</Text>
            </TouchableOpacity>
          </View>
        </View>
      </SafeAreaView>
    </>
  );
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    marginTop: 20,
  },
  container: {
    marginHorizontal: 30,
    justifyContent: 'space-between',
    flex: 1
  },
  textInput: {
    height: 45,
    color: "#333333",
    paddingHorizontal: 15,
    fontSize: 16,
    paddingBottom: 10,
    borderColor: "#cdcdcf",
    borderWidth: 1,
  },
  phoneInput: {
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3,
  },
  passwordInput: {
    borderTopWidth: 0,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3,
  },
  loginButton: {
    height: 40,
    backgroundColor: "#1977f3",
    borderRadius: 6,
    marginVertical: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  newAccount: {
    height: 40,
    backgroundColor: "#e7f3ff",
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: "100%",
    marginBottom: 15,
  },
  optionRowText: {
    color: "#0288d1",
    fontSize: 16,
    fontWeight: "500",
  },
  loginText: {
    fontSize: 16,
    color: "#fff"
  },
  image: {
    width: "100%",
    height: null,
    aspectRatio: 750 / 460,
  },
  divider: {
    borderWidth: 1,
    borderColor: "#cbccd0",
    flex: 1,
  },
  dividerText: {
    color: "#212121",
    textAlign: 'center',
    width: 50
  },
  footer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: "70%",
    marginBottom: 10,
  }
});
