import React from 'react';
import Constants from "expo-constants";
import { StyleSheet, Text, View } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export default function HelloWorld2() {
  return (
    <View style={styles.container}>
      <Text style={styles.textForm}>Hello World!</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2980b9',
    alignItems: 'center',
    justifyContent: 'center',
    // SafeAreaView on Android devices
    paddingTop: Constants.statusBarHeight,
  },
  textForm: {
    textAlign: "center",
    fontSize: 30,
    fontWeight: 'bold',
    textTransform: "uppercase",
    color: "white",
  },
});
