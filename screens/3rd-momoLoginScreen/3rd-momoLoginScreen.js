import React from 'react';
import Constants from "expo-constants";
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, TextInput} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';


export default function MomoLogin() {
  return (
    <SafeAreaView style={styles.container}>
    <StatusBar style="light" />

      <View>
        <Text style={styles.welcomeTextStyles}>Xin Chào !</Text>
        <Text style={styles.nameTextStyles}>Nguyen Duy Khanh</Text>
        <Text style={styles.numberTextStyles}>09xxxxx403</Text>
      </View>

      <View style={{marginHorizontal: 20}}>
        <View>
          <FontAwesome5 name="lock" size={24} color="black" style={styles.iconLock} />
          <TextInput
            style={styles.textInput}
            keyboardType="numeric"
            autoFocus={true}
            placeholder="Nhập mật khẩu"
            placeholderTextColor="#929292"
            secureTextEntry={true}
          />
          <TouchableOpacity style={styles.loginButton}>
            <Text style={styles.nameTextStyles}>đăng nhập</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: "row", justifyContent: "space-between", marginTop: 30}}>
          <TouchableOpacity>
            <Text style={styles.nameTextStyles}>quên mật khẩu ?</Text>
          </TouchableOpacity>
          <TouchableOpacity>
            <Text style={styles.nameTextStyles}>thoát tài khoản</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const TextStyles = {
  textAlign: "center",
  color: "#fff",
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#b0006d",
    paddingTop: Constants.statusBarHeight, // SafeAreaView on Android devices
  },
  numberTextStyles: {
    ...TextStyles,
    fontSize: 16,
  },
  welcomeTextStyles: {
    ...TextStyles,
    fontSize: 18,
    lineHeight: 50,
    fontWeight: 'bold',
  },
  nameTextStyles: {
    ...TextStyles,
    fontSize: 14,
    lineHeight: 30,
    textTransform: "uppercase",
  },
  textInput: {
    height: 50,
    backgroundColor: "#fff",
    borderRadius: 25,
    marginTop: 30,
    textAlign: "center",
    fontSize: 18,
    textAlignVertical: "center"
  },
  loginButton: {
    height: 40,
    backgroundColor: "#8d015a",
    borderRadius: 25,
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  iconLock: {
    position: 'absolute',
    top: 45,
    left: 25,
    zIndex: 1,
    color: "#929292",
    fontSize: 16
  }
});
