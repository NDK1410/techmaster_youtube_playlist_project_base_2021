import { StatusBar } from 'expo-status-bar';
import Constants from "expo-constants";
import React from 'react';
import { StyleSheet, SafeAreaView, Text } from 'react-native';

export default function HelloWorld1() {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.textForm}>
        Hello <Text style={styles.firstWorld}>World</Text>
      </Text>
      <Text style={styles.textForm}>
        Hello{" "}<Text style={styles.secondWorld}>World</Text>
      </Text>
      <Text style={[styles.textForm, styles.thirdHello]}>
        Hello{" "}
        <Text style={styles.thirdWorld}>World</Text>
      </Text>
      <StatusBar style="auto" />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    // SafeAreaView on Android devices
    paddingTop: Constants.statusBarHeight,
  },
  textForm: {
    marginTop: 50,
    textAlign: "center",
    fontSize: 30,
    fontWeight: 'bold',
    textTransform: "uppercase",
  },
  firstWorld: {
    color: "#e74c3c",
  },
  secondWorld: {
    fontStyle: "italic",
    textDecorationLine: "underline",
    color: "#2980b9",
  },
  thirdWorld: {
    backgroundColor: "#59595d",
    color: "#fff",
  },
  thirdHello: {
    color: "#59595d",
  },
});
